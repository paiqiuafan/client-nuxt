module.exports = {
    plugins: [
        'html',
      'vue'	
    ],
    rules: {
    'vue/no-parsing-error': [2, { 'x-invalid-end-tag': true }]
    }
}