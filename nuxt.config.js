const path = require('path');

module.exports = {
  mode: 'universal',
  srcDir: 'src',
//   buildDir: 'dist',
  /*
  ** Headers of the page
  */
  head: {
    title: '早苗帆布',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
    script: [
      {
        src: 'https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js',
        'data-ad-client': "ca-pub-1501815300572667",
        async: true
      }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
    "~assets/scss/index.scss"
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    // '@/plugins/iview'
    {src: '~plugins/iview', ssr: true},
    {src: '~plugins/firebase', ssr: true},
    {src: '~/plugins/vue-fb-customer-chat.js', ssr: false}
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/axios'
  ],
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
      config.resolve.alias['@'] = path.join(__dirname,'src')
      config.resolve.alias['@U'] = path.join(__dirname, 'src/utils')
    }
  },
  // styleResources: {
  //   scss: ["~assets/scss/index.scss"]
  // }
}
