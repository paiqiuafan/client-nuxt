import firebase from 'firebase/app';
import 'firebase/database';
import 'firebase/storage';

let firebaseInit = null;

const firebaseConfig = {
    apiKey: "AIzaSyCEk3elifJJ6BnNtadMnOIYoi8vOVw7tjg",
    authDomain: "admin-simple.firebaseapp.com",
    databaseURL: "https://admin-simple.firebaseio.com",
    projectId: "admin-simple",
    storageBucket: "admin-simple.appspot.com",
    messagingSenderId: "536650248828",
    appId: "1:536650248828:web:9fdf7a23f5b5181bc3cff9",
    measurementId: "G-MP07S17PMR"
};

export default function () {
    firebaseInit = firebase.apps.length
        ? firebase.app()
        : firebase.initializeApp(firebaseConfig);
    return firebaseInit;
}
