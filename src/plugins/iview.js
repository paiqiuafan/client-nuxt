import Vue from 'vue'
// import iView from 'iview'
import locale from 'iview/dist/locale/en-US' // Change locale, check node_modules/iview/dist/locale
import {
  Header,
  Layout,
  Icon,
  InputNumber,
  Badge,
  Button,
  Carousel,
  CarouselItem,
  Content,
  Card,
  Col,
  Collapse,
  Drawer,
  Divider,
  Footer,
  Option,
  Panel,
  Row,
  Select
 } from 'iview'

Vue.component('Header', Header);
Vue.component('Layout', Layout);
Vue.component('InputNumber', InputNumber);
Vue.component('Icon', Icon);
Vue.component('Badge', Badge);
Vue.component('Button', Button);
Vue.component('Carousel', Carousel);
Vue.component('CarouselItem', CarouselItem);
Vue.component('Content', Content);
Vue.component('Card', Card);
Vue.component('Col', Col);
Vue.component('Collapse', Collapse);
Vue.component('Drawer', Drawer);
Vue.component('Divider', Divider);
Vue.component('Footer', Footer);
Vue.component('Option', Option);
Vue.component('Panel', Panel);
Vue.component('Row', Row);
Vue.component('Select', Select);

// Vue.use(iView, {
//   locale
// })

Vue.locale = locale;
