import Vue from 'vue'
import VueFbCustomerChat from 'vue-fb-customer-chat'
 
Vue.use(VueFbCustomerChat, {
  page_id: "248687915510216", // 輸入 Facebook Page ID,
  locale: 'zh_TW', // 設定語言
  logged_out_greeting:"您好，有任何問題歡迎留言詢問，小編將速速回覆，尖峰時間若久未回覆請還請各位見諒😊 📌聊聊客服回覆時間：週一至週日 10:00~18:00", //登入狀態歡迎詞
  logged_in_greeting:"您好，有任何問題歡迎留言詢問，小編將速速回覆，尖峰時間若久未回覆請還請各位見諒😊 📌聊聊客服回覆時間：週一至週日 10:00~18:00" //登出(未登入)狀態歡迎詞
})
