import * as types from './mutations_type.js'

const state = {
    showDrawer: false,
    showDrawerOption: {
        show: false,
        title: ''
    }
};

const actions = {
    setShowDrawer({ commit }, payload) {
        commit(types.SET_SHOW_DRAWER, payload);
    },
    setShowDrawerOption({ commit }, payload) {
        commit(types.SET_SHOW_DRAWER_OPTION, payload);
    }
}

const mutations = {
    [types.SET_SHOW_DRAWER](state, payload) {
        state.showDrawer = payload;
    },
    [types.SET_SHOW_DRAWER_OPTION](state, payload) {
        state.showDrawerOption = { ...state.showDrawerOption, ...payload };
    }
}

const getters = {
    showDrawer: state => state.showDrawer,
    showDrawerOption: state => state.showDrawerOption
}

export default {
    state,
    actions,
    mutations,
    getters
}