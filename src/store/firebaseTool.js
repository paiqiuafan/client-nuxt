import Vue from 'vue';
import firebase from '~/plugins/firebase.js';

const storage = firebase().storage();

// 更新圖片
export const updateImage = ({ commit }, payload) => {
  const { id, path, file, oldFile } = payload
  return storage.ref().child(`${path}/${id}_${file.name}`).put(file, {
    contentType: file.type
  }).then(() => storage.ref().child(`${path}/${oldFile.name}`).delete())
}

// 新增圖片
export const createImage = ({ commit }, payload) => {
  const { id, path, file } = payload
  return storage.ref().child(`${path}/${id}_${file.name}`).put(file, {
    contentType: file.type
  })
}

// 刪除圖片
export const deleteImage = ({ commit }, payload) => {
  const { path } = payload
  return storage.ref().child(path).delete()
}

// 取圖片
export const fetchImageUrl = ({ commit }, payload) => {
  const { path } = payload
  return storage.ref().child(path).getDownloadURL()
}
