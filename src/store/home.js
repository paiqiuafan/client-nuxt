import * as types from './mutations_type.js';
import firebase from '~/plugins/firebase.js';

const DB = firebase().database();

const state = () => ({
    selectedProductId: '',
    products: {
        hot: {},
        new: {},
    },
    carousel: {
        placard: {},
        banner: {}
    },
    viewersCount: 0
});

const actions = {
    async fetchHomeData({ commit }) {
        await DB.ref('Products').orderByChild('isNew').equalTo(true).once("value").then((res) => {
            commit(types.SET_HOME_PRODUCTS, { new: res.val() });
        });

        await DB.ref('Products').orderByChild('isHot').equalTo(true).once("value").then((res) => {
            commit(types.SET_HOME_PRODUCTS, { hot: res.val() });
        });

        await DB.ref('CarouselSetting/Placard')
                .orderByChild('enable')
                .equalTo(true).once("value").then((res) => {
                    commit(types.SET_HOME_CAROUSEL, { placard: res.val() });
                });

        await DB.ref('CarouselSetting/Banner')
                .orderByChild('sort')
                .once("value", (res) => {
                    const orderResult = [];
                    res.forEach((item) => {
                        item.val().enable && orderResult.push(item.val());
                    })
                    commit(types.SET_HOME_CAROUSEL, { banner: orderResult });
                });
    },
    fetchProductById({ commit }, payload) {
        return DB.ref('Products')
                .orderByChild('id')
                .equalTo(payload)
                .once("value");
    },
    fetchBanner({ commit }) {
        return DB.ref('CarouselSetting/Banner')
                .orderByChild('sort')
                .once("value")
    },
    setSelectedProductId({ commit }, payload) {
        commit(types.SET_SELECTED_PRODUCT_ID, payload);
    },
    async fetchViewersCount({ commit, dispatch }) {
        await DB.ref('Dashboard').once("value").then((res) => {
            const { viewersCount } = JSON.parse(JSON.stringify(res.toJSON()));
            commit(types.SET_VIEWERS_COUNT, viewersCount);
        });
        
    },
    async increaseViewer({ state, commit }, payload) {
        await DB.ref('Dashboard').set({
            viewersCount: payload
        });
        commit(types.SET_VIEWERS_COUNT, payload);
    }
}

const mutations = {
    [types.SET_SELECTED_PRODUCT_ID](state, payload) {
        state.selectedProductId = payload;
    },
    [types.SET_HOME_PRODUCTS](state, payload) {
        state.products = { ...state.products, ...payload };
    },
    [types.SET_HOME_CAROUSEL](state, payload) {
        state.carousel = { ...state.carousel, ...payload };
    },
    [types.SET_VIEWERS_COUNT](state, payload) {
        
        if (payload) {
            state.viewersCount = payload;
        }
    }
}

const getters = {
    products: state => state.products,
    selectedProductId: state => state.selectedProductId,
    carousel: state => state.carousel,
    viewersCount: state => state.viewersCount
}

export default {
    state,
    actions,
    mutations,
    getters
}
