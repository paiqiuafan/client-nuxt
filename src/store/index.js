import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);

import app from './app.js';
import home from './home.js';
import shopCart from './shopCart.js';

export const store = () =>  new Vuex.Store({
    modules: {
        app,
        home,
        shopCart
    }
  })

export default store;