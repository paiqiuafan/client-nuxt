import * as types from './mutations_type.js';
import firebase from '~/plugins/firebase.js';
import { CACHE_SHOP_CART } from '@U/constants';

const DB = firebase().database();

const state = {
    cartList: []
};

const actions = {
    increaseCart({ commit }, payload) {
        commit(types.INCREASE_CART, payload);
    }
}

const mutations = {
    [types.INCREASE_CART](state, payload) {
        state.cartList.push(payload);
    }
}

const getters = {
    cartList: state => state.cartList
}

export default {
    state,
    actions,
    mutations,
    getters
}
